use futures::{Future, Stream, Poll, Async};
use hyper;
use regex;

use downloader;
use errors;
use utils;

lazy_static! {
    static ref REDDIT_URL_PAT: regex::Regex =
        regex::Regex::new(r"^https?://www.reddit.com/user/(\S+)$").unwrap();
}

/// A reddit link
/// https://github.com/reddit-archive/reddit/wiki/JSON#link-implements-votable--created
#[derive(Debug, Deserialize, Serialize, Clone)]
struct Link {
    pub archived: bool,
    pub author: String,
    pub author_flair_type: String,
    pub can_gild: bool,
    pub can_mod_post: bool,
    pub clicked: bool,
    pub contest_mode: bool,
    pub created: f64,
    pub created_utc: f64,
    pub domain: String,
    pub downs: i64,
    //pub edited: bool,
    pub gilded: i64,
    pub hidden: bool,
    pub hide_score: bool,
    pub id: String,
    pub is_crosspostable: bool,
    pub is_meta: bool,
    pub is_original_content: bool,
    pub is_reddit_media_domain: bool,
    pub is_self: bool,
    pub is_video: bool,
    pub link_flair_background_color: String,
    pub link_flair_text_color: String,
    pub link_flair_type: String,
    pub locked: bool,
    pub media_only: bool,
    pub name: String,
    pub no_follow: bool,
    pub num_comments: i64,
    pub num_crossposts: i64,
    pub over_18: bool,
    pub parent_whitelist_status: Option<String>,
    pub permalink: String,
    pub pinned: bool,
    pub pwls: Option<i64>,
    pub quarantine: bool,
    // pub rte_mode: String,
    pub saved: bool,
    pub score: i64,
    pub selftext: Option<String>,
    pub selftext_html: Option<String>,
    pub send_replies: bool,
    pub spoiler: bool,
    pub stickied: bool,
    pub subreddit: String,
    pub subreddit_id: String,
    pub subreddit_name_prefixed: String,
    pub subreddit_subscribers: i64,
    pub subreddit_type: String,
    pub thumbnail: String,
    pub title: String,
    pub ups: i64,
    pub url: String,
    pub visited: bool,
    pub whitelist_status: Option<String>,
    pub wls: Option<i64>,
}

impl Link {
    fn for_username(username: &str) -> LinkStream {
        LinkStream {
            username: username.to_string(),
            request: None,
            listing: None,
        }
    }
}


pub struct RedditUserSubmissions {
    link: String,
    username: String,
}

fn username_from_link(link: &str) -> Option<&str> {
    REDDIT_URL_PAT.captures(link)
        .and_then(|groups| {
            groups.get(1)
                .map(|username| {
                    username.as_str()
                })
        })
}

impl downloader::FromLink for RedditUserSubmissions {
    fn from_link(link: &str) -> Option<Self> {
        username_from_link(link)
            .map(|username| {
                RedditUserSubmissions {
                    link: link.to_string(),
                    username: username.to_string(),
                }
            })
    }
}

impl downloader::LinkProducer for RedditUserSubmissions {
    fn get_links(&self) -> Box<Stream<Item=String, Error=errors::FetchrsError>> {
        Box::new(get_urls_for_username(&self.username))
    }
}

struct LinkStream {
    username: String,
    request: Option<Box<Future<Item=ListingContainer, Error=errors::FetchrsError>>>,
    listing: Option<(usize, Listing)>,
}

impl Stream for LinkStream {
    type Item = Link;
    type Error = errors::FetchrsError;

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        let future = match self.listing {
            Some((ref mut next_idx, ref listing)) => {
                // case: listing already fetched
                if *next_idx < listing.children.len() {
                    // case: haven't yet exhausted fetched listings
                    let link = listing.children[*next_idx].data.clone();
                    *next_idx += 1;
                    return Ok(Async::Ready(Some(link)))
                } else {
                    // case: fetched listings exhausted
                    match listing.after {
                        Some(ref after) => {
                            // case: more listings exist that can be fetched
                            get_listing(&self.username, Some(&after))
                        },
                        None => {
                            // case: no more listings exist that can be fetched
                            return Ok(Async::Ready(None));
                        }
                    }
                }
            },
            None => {
                // case: listing not yet fetched
                match self.request {
                    Some(ref mut future) => {
                        // case: request has been issued
                        match future.poll() {
                            Ok(Async::Ready(listing_container)) => {
                                // case: request has been returned
                                let next_idx = 0;
                                let listing = listing_container.data;

                                if next_idx < listing.children.len() {
                                    // case: haven't yet exhausted fetched listings
                                    let link = listing.children[next_idx].data.clone();
                                    self.listing = Some((next_idx + 1, listing));
                                    return Ok(Async::Ready(Some(link)))
                                } else {
                                    // case: fetched listings exhausted
                                    match listing.after {
                                        Some(ref after) => {
                                            // case: more listings exist that can be fetched
                                            get_listing(&self.username, Some(&after))
                                        },
                                        None => {
                                            // case: no more listings exist that can be fetched
                                            return Ok(Async::Ready(None));
                                        }
                                    }
                                }

                            },
                            Ok(Async::NotReady) => {
                                // case: request hasn't finished yet
                                return Ok(Async::NotReady);
                            },
                            Err(e) => {
                                // case: request errored out
                                return Err(e);
                            },
                        }
                    },
                    None => {
                        // case: request hasn't yet been issued
                        get_listing(&self.username, None)
                    }
                }
            }
        };

        self.request = Some(Box::new(future));
        self.listing = None;
        self.poll()
    }
}

pub fn get_urls_for_username(username: &str) -> impl Stream<Item=String, Error=errors::FetchrsError> {
    Link::for_username(username).map(|l| l.url )
}

#[derive(Debug, Deserialize, Serialize, Clone)]
struct LinkContainer {
    kind: String,
    data: Link,
}

#[derive(Debug, Deserialize, Serialize)]
struct Listing {
    modhash: String,
    dist: i64,
    children: Vec<LinkContainer>,
    after: Option<String>,
    before: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct ListingContainer {
    kind: String,
    data: Listing,
}

fn get_listing(username: &str, after: Option<&str>) -> impl Future<Item=ListingContainer, Error=errors::FetchrsError> {
    let uri: hyper::Uri = match after {
        Some(after_idx) => {
            format!("https://api.reddit.com/user/{}/submitted?after={}", username, after_idx)
        },
        None => format!("https://api.reddit.com/user/{}/submitted", username)
    }.parse().unwrap();

    let request = hyper::Request::get(uri)
        .header("User-Agent", utils::USER_AGENT)
        .body(hyper::Body::empty())
        .expect("unable to construct HTTP request");

    utils::get(request)
}

#[cfg(test)]
mod test {
    use super::*;

    use serde_json;
    use tokio;

    static USERNAME: &str = "wilsoniya";

    #[test]
    fn test_link_deserialize() {
        let link_json_str = include_str!("link.json");
        let l = serde_json::from_str::<Link>(link_json_str)
            .expect("could not deserialize Link");
        assert!(USERNAME == l.author);
    }

    #[test]
    fn test_listing_deserialize() {
        let listing_json_str = include_str!("listing.json");
        let l = serde_json::from_str::<Listing>(listing_json_str)
            .expect("could not deserialize Listing");
        assert!("t3" == l.children[0].kind);
        assert!(USERNAME == l.children[0].data.author);
    }

    #[test]
    fn test_listing_container_deserialize() {
        let listing_container_json_str = include_str!("listing_container.json");
        let l = serde_json::from_str::<ListingContainer>(listing_container_json_str)
            .expect("could not deserialize ListingContainer");
        assert!("Listing" == l.kind);
        assert!("t3" == l.data.children[0].kind);
        assert!(USERNAME == l.data.children[0].data.author);
    }

    #[test]
    fn test_get_listing() {
        run_future!({
            get_listing(USERNAME, None)
                .map_err(|e| {
                    panic!("error getting listing {:?}", e);
                })
        })
    }

    #[test]
    fn test_link_stream() {
        run_future!({
            Link::for_username(USERNAME)
                .collect()
                .map_err(|e| {
                    panic!("error collecting links: {:?}", e);
                })
        });
    }

    #[test]
    fn test_get_urls_for_username() {
        run_future!({
            get_urls_for_username(USERNAME)
                .collect()
                .map_err(|e| {
                    panic!("error collecting urls: {:?}", e);
                })
        });
    }

    #[test]
    fn test_username_from_link() {
        // valid https link
        let link = "https://www.reddit.com/user/fart";
        let expected = Some("fart");
        let actual = username_from_link(link);
        assert_eq!(expected, actual);

        // valid http link
        let link = "http://www.reddit.com/user/fart";
        let expected = Some("fart");
        let actual = username_from_link(link);
        assert_eq!(expected, actual);

        // invalid link; no username given
        let link = "https://www.reddit.com/user/";
        let expected = None;
        let actual = username_from_link(link);
        assert_eq!(expected, actual);

        // invalid domain
        let link = "https://www.bogus.com/user/fart";
        let expected = None;
        let actual = username_from_link(link);
        assert_eq!(expected, actual);
    }
}
