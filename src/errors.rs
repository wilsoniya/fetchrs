use std;

use hyper;
use serde_json;

#[derive(Debug)]
pub enum FetchrsError {
    RequestError(hyper::Error),
    Utf8Error(::std::str::Utf8Error),
    DeserializationError(serde_json::Error),
    FileError(std::io::Error),
//  GenericError(String),
}
