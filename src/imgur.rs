use futures::{self, Future};
use futures::Stream;
use hyper;
use regex;

use downloader;
use errors;
use utils;

static CLIENT_ID: &str = "06308452b1b449d";

lazy_static! {
    static ref IMGUR_URL_PAT: regex::Regex =
        regex::Regex::new(r"^https?://imgur\.com/gallery/(\S+)$").unwrap();
}

#[derive(Debug, Deserialize, Serialize)]
struct AlbumContainer {
    data: Album,
}

#[derive(Debug, Deserialize, Serialize)]
struct Album {
    account_id: i64,
    account_url: String,
    cover: String,
    cover_height: i64,
    cover_width: i64,
    datetime: i64,
    description: Option<String>,
    favorite: bool,
    id: String,
    images: Vec<Image>,
    images_count: i64,
    in_gallery: bool,
    layout: String,
    link: String,
    nsfw: bool,
    privacy: String,
    section: String,
    title: String,
    views: i64,
}

#[derive(Debug, Deserialize, Serialize)]
struct Image {
    bandwidth: i64,
    datetime: i64,
    description: Option<String>,
    favorite: bool,
    height: i64,
    id: String,
    in_gallery: bool,
    link: String,
    nsfw: Option<bool>,
    section: Option<String>,
    size: i64,
    title: Option<String>,
    #[serde(rename = "type")]
    type_: String,
    views: i64,
    vote: Option<String>,
    width: i64,
}

fn get_album(album_hash: &str) -> impl Future<Item=AlbumContainer, Error=errors::FetchrsError> {
    let uri: hyper::Uri = format!("https://api.imgur.com/3/album/{}", album_hash)
        .parse().unwrap();

    let request = hyper::Request::get(uri)
        .header("User-Agent", utils::USER_AGENT)
        .header("Authorization", format!("Client-ID {}", CLIENT_ID).as_str())
        .body(hyper::Body::empty())
        .expect("unable to construct HTTP request");

    utils::get(request)
}

fn get_image_urls_for_album(album_hash: &str) ->
impl Future<Item=Vec<String>, Error=errors::FetchrsError> {
    get_album(album_hash)
        .map(|album_container| {
            album_container.data.images
                .iter()
                .map(|image| image.link.clone())
                .collect()
        })
}

fn get_image_stream_for_album(album_hash: &str) -> impl futures::Stream<Item=String, Error=errors::FetchrsError> {
    get_image_urls_for_album(album_hash)
        .into_stream()
        .map(|urls| {
            futures::stream::iter_ok(urls)
        })
        .flatten()
}

fn album_hash_from_link(link: &str) -> Option<&str> {
    IMGUR_URL_PAT.captures(link)
        .and_then(|groups| {
            groups.get(1)
                .map(|album_hash| {
                    album_hash.as_str()
                })
        })
}

pub struct ImgurGallery {
    link: String,
    album_hash: String,
}

impl downloader::FromLink for ImgurGallery {
    fn from_link(link: &str) -> Option<Self> {
        album_hash_from_link(link)
            .map(|album_hash| {
                ImgurGallery {
                    link: link.to_string(),
                    album_hash: album_hash.to_string(),
                }
            })
    }
}

impl downloader::LinkProducer for ImgurGallery {
    fn get_links(&self) -> Box<futures::Stream<Item=String, Error=errors::FetchrsError>> {
        Box::new(get_image_stream_for_album(&self.album_hash))
    }
}

#[cfg(test)]
mod test {
    use serde_json;
    use tokio;

    use super::*;

    static ALBUM_HASH: &str = "gyCvp";
    static NUM_ALBUM_LINKS: usize = 50;

    #[test]
    fn test_album_container_deserialize() {
        let album_container_json_str = include_str!("imgur_album.json");
        let _ = serde_json::from_str::<AlbumContainer>(album_container_json_str)
            .expect("could not deserialize AlbumContainer");
    }

    #[test]
    fn test_get_album() {
        run_future!({
            get_album(ALBUM_HASH)
                .map(|album_container| {
                    let expected = NUM_ALBUM_LINKS;
                    let actual = album_container.data.images.len();
                    assert!(expected == actual);
                })
        });
    }

    #[test]
    fn test_get_image_urls_for_album() {
        run_future!({
            get_image_urls_for_album(ALBUM_HASH)
                .map(|urls| {
                    let expected = NUM_ALBUM_LINKS;
                    let actual = urls.len();
                    assert!(expected == actual);
                })
        });
    }

    #[test]
    fn test_album_hash_from_link() {
        // valid https link
        let link = "https://imgur.com/gallery/fart";
        let expected = Some("fart");
        let actual = album_hash_from_link(link);
        assert_eq!(expected, actual);

        // valid http link
        let link = "http://imgur.com/gallery/fart";
        let expected = Some("fart");
        let actual = album_hash_from_link(link);
        assert_eq!(expected, actual);

        // invalid link; no album hash given
        let link = "http://imgur.com/gallery/";
        let expected = None;
        let actual = album_hash_from_link(link);
        assert_eq!(expected, actual);

        // invalid domain
        let link = "http://pictures.com/gallery/fart";
        let expected = None;
        let actual = album_hash_from_link(link);
        assert_eq!(expected, actual);
    }
}
