use std::marker::Sized;

use futures;

use errors;
use imgur::ImgurGallery;
use reddit::RedditUserSubmissions;

pub enum Content {
    Leaf(Box<Downloadable>),
    Directory(Box<LinkProducer>),
}

/// A type which can optionally be constructed given a link string.
pub trait FromLink {
    fn from_link(link: &str) -> Option<Self> where Self: Sized;
}

/// A thing which represents downloadable content.
pub trait Downloadable {
    fn download(&self);
}

/// A thing which represents content which points to other content.
pub trait LinkProducer {
    fn get_links(&self) -> Box<futures::Stream<Item=String, Error=errors::FetchrsError>>;
}

pub fn classify_link(link: &str) -> Option<Content> {
    if let Some(link_producer) = RedditUserSubmissions::from_link(link) {
        Some(Content::Directory(Box::new(link_producer)))
    } else if let Some(link_producer) = ImgurGallery::from_link(link) {
        Some(Content::Directory(Box::new(link_producer)))
    } else {
        None
    }
}

pub fn download(link: &str) {
}
