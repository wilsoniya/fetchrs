#![macro_use]
#![cfg(test)]


macro_rules! run_future {
    ( $body:block ) => {
        {
            let mut rt = tokio::runtime::current_thread::Runtime::new()
                .expect("could not create current_thread runtime");

            let future = $body;

            rt.spawn(
                future
                .map(|_| { () })
                .map_err(|_| { () }));
            let _ = rt.run();
        }
    };
}
