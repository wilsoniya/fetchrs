extern crate futures;
extern crate hyper;
extern crate hyper_tls;
#[macro_use] extern crate lazy_static;
extern crate regex;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate tokio;

// macro definitions within; must come first
mod test_utils;

mod downloader;
mod errors;
mod imgur;
mod reddit;
mod utils;

fn main() {
    println!("hello, world!");
}
