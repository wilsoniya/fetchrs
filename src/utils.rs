use futures::{Future, Stream};
use hyper;
use hyper_tls;
use serde::de::DeserializeOwned;
use serde_json;

use std::io::Write;
use std::marker::Send;

use errors;

pub static USER_AGENT: &'static str = "fetchrs/0.1.0";
pub static NUM_DNS_THREADS: usize = 4;

/// Executes the given `request`, dezerializing the response into the type `T`.
///
/// ## Parameters
/// * `request` - the HTTP request to execute
///
/// ## Type Parameters
/// * `T` - the type into which the response body should be instantiated
pub fn get<T>(request: hyper::Request<hyper::Body>) -> impl Future<Item=T, Error=errors::FetchrsError>
where T: DeserializeOwned {
    // TODO: maybe this should be configurable
    let https = hyper_tls::HttpsConnector::new(NUM_DNS_THREADS)
        .expect("TLS initialization failed");
    let client = hyper::Client::builder()
        .build::<_, hyper::Body>(https);

    client
        .request(request)
        .map_err(|e|{
            errors::FetchrsError::RequestError(e)
        })
        .and_then(|res| {
            res
                .into_body()
                .concat2()
                .map_err(|e|{
                    errors::FetchrsError::RequestError(e)
                })
        })
    .and_then(|ref body_bytes| {
        ::std::str::from_utf8(body_bytes)
            .map(|bytes| { bytes.to_owned() })
            .map(|s| { s })
            .map_err(|e|{
                errors::FetchrsError::Utf8Error(e)
            })
    })
    .and_then(|ref body| {
        serde_json::from_str::<T>(body)
            .map_err(|e|{
                errors::FetchrsError::DeserializationError(e)
            })
    })
}

/// Downloads the resource pointed to by `request`, writing fetched content to `writer`.
///
/// ## Parameters
/// * `request` - HTTP request to execute
/// * `writer` - thing to which fetched bytes will be writeen
///
/// ## Type Parameters
/// `T` - a thing which can be written to
///
/// ## Returns
/// Number of fetched bytes on success
fn download<T: Write + Send>(request: hyper::Request<hyper::Body>, mut writer: T)
    -> impl Future<Item=usize, Error=errors::FetchrsError> {

        let https = hyper_tls::HttpsConnector::new(NUM_DNS_THREADS)
            .expect("TLS initialization failed");
        let client = hyper::Client::builder()
            .build::<_, hyper::Body>(https);

        client
            .request(request)
            .map_err(errors::FetchrsError::RequestError)
            .and_then(|response| {
                response
                    .into_body()
                    .map_err(errors::FetchrsError::RequestError)
                    .map(move |chunk| {
                        writer.write(&chunk)
                            .map_err(errors::FetchrsError::FileError)
                    })
                    .fold(0, |count1, count2_or_err| {
                        count2_or_err.map(|count2| count1 + count2)
                    })
            })
}


#[cfg(test)]
mod test {
    use super::*;

    use hyper;
    use tokio;

    #[test]
    fn test_download() {
        let uri: hyper::Uri = "https://i.imgur.com/vVy5wSV.png"
            .parse().unwrap();

        let request = hyper::Request::get(uri)
            .body(hyper::Body::empty())
            .expect("unable to construct HTTP request");

        let writer: Vec<u8> = Vec::new();

        run_future!({
            download(request, writer)
                .map(move |num_downloaded_bytes| {
                    let expected = 392404;
                    assert_eq!(expected, num_downloaded_bytes);
                })
               .map_err(|err| {
                   panic!("encountered error resolving future: {:?}", err);
               })
        });
    }
}
